package daoi;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import util.Conexion;
import model.Aula;
import model.Distrito;
import model.Docente;
import daol.DocenteDAOL;

public class DocenteDAOI  implements DocenteDAOL {

	Conexion cn = new Conexion();
	
	@Override
	public void insertar(Docente obj) throws Exception {
		cn.abrir();
		
		try {
			cn.em.getTransaction().begin();
			cn.em.persist(obj);
			cn.em.getTransaction().commit();
		} catch (Exception e) {
			cn.em.getTransaction().rollback();
			System.out.println("Error en insertar \n" + e.getMessage());
		}
		
	}

	@Override
	public void editar(Docente obj) throws Exception {
	cn.abrir();
		
		try {
			cn.em.getTransaction().begin();
			cn.em.merge(obj);
			cn.em.getTransaction().commit();
		} catch (Exception e) {
			cn.em.getTransaction().rollback();
			System.out.println("Error en editar \n" + e.getMessage());
		}
		
	}

	@Override
	public void eliminar(Docente obj) throws Exception {
		cn.abrir();
		try {
			cn.em.getTransaction().begin();
			Docente oDocente=cn.em.find(Docente.class, obj.getIdDocente());
			cn.em.remove(oDocente);
			cn.em.getTransaction().commit();
		} catch (Exception e) {
			cn.em.getTransaction().rollback();
			System.out.println("Error en eliminar\n " + e.getMessage());
		}
		
	}

	@Override
	public List<Docente> buscarPorId(Docente obj) throws Exception {
		cn.abrir();
		Query q = cn.em.createQuery("select d, d.aula.idAula, d.distrito.idDistrito from Docente d where d.idDocente = :idDocente");
		q.setParameter("idDocente", obj.getIdDocente());
		
		List<Object[]> lista_objetos =q.getResultList();
		List<Docente> lista = new ArrayList<Docente>();
		for(Object objs[]: lista_objetos){
			Docente docente=(Docente) objs[0];
			lista.add(docente);
		}
		return lista;
	}

	@Override
	public List<Docente> buscarPorAula(Docente obj) throws Exception {
		cn.abrir();
		Query q = cn.em.createQuery("select d, d.aula.idAula, d.distrito.idDistrito from Docente d where d.aula.idAula = :idAula");
		q.setParameter("idAula", obj.getAula().getIdAula());
		
		List<Object[]> lista_objetos =q.getResultList();
		List<Docente> lista = new ArrayList<Docente>();
		for(Object objs[]: lista_objetos){
			Docente docente=(Docente) objs[0];
			lista.add(docente);
		}
		return lista;
	}

	@Override
	public List<Docente> buscarPorDistrito(Docente obj) throws Exception {
		cn.abrir();
		Query q = cn.em.createQuery("select d, d.aula.idAula, d.distrito.idDistrito from Docente d where d.distrito.idDistrito = :idDistrito");
		q.setParameter("idDistrito", obj.getDistrito().getIdDistrito());
		
		List<Object[]> lista_objetos =q.getResultList();
		List<Docente> lista = new ArrayList<Docente>();
		for(Object objs[]: lista_objetos){
			Docente docente=(Docente) objs[0];
			lista.add(docente);
		}
		return lista;
	}

	@Override
	public List<Distrito> listarDistritos() throws Exception {
		cn.abrir();
		List<Distrito> lista = cn.em.createQuery("select d from Distrito d").getResultList(); 
		return lista;
	}

	@Override
	public List<Aula> listarAulas() throws Exception {
		cn.abrir();
		List<Aula> lista = cn.em.createQuery("select a from Aula a").getResultList(); 
		return lista;
	}

	
}
