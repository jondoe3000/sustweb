package bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;

import daoi.DocenteDAOI;
import model.Docente;
import model.Aula;
import model.Distrito;


public class DocenteBean {

	public int idDocente;
	public String nombres;
	public int edad;
	public double talla;
	public Aula aula;
	public Distrito distrito;
	public Date fechaRegistro;
	
	private DocenteDAOI oDocenteDAOI = new DocenteDAOI();
	
	private List<Docente> listaDocente;
	private List<Aula> listaAula;
	private List<Distrito> listaDistrito;
	
	
	public int listaDocente_size;
	public int filaSeleccionada;
	public String opcionSeleccionada="ID";
	
	public boolean sw_edit =false;
	
	public void accionEditar() throws Exception{
		sw_edit=true;
		llenaCombo();
	}
	
	public void accionInsertar() throws Exception{
		sw_edit=false;
		llenaCombo();
	}
	public void limpiar(){
		
	}
	
	public void llenaCombo() throws Exception{
		listaAula = oDocenteDAOI.listarAulas();
		listaDistrito = oDocenteDAOI.listarDistritos();
	}
	
	public void buscar() throws Exception{
		Docente oDocente= new Docente();
		listaDocente = new ArrayList<Docente>();
		
		if(opcionSeleccionada.equals("ID")){
			oDocente.setIdDocente(getIdDocente());
			listaDocente=oDocenteDAOI.buscarPorId(oDocente);
		}else if (opcionSeleccionada.equals("AULA")){
			oDocente.setAula(getAula());
			listaDocente=oDocenteDAOI.buscarPorAula(oDocente);
		}else if (opcionSeleccionada.equals("DISTRITO")){
			oDocente.setDistrito(getDistrito());
			listaDocente=oDocenteDAOI.buscarPorDistrito(oDocente);
		}
		listaDocente_size=listaDocente.size();
	}
	
	public void insertar() throws Exception{
		Docente oDocente = new Docente();
		oDocente.setIdDocente(getIdDocente());
		oDocente.setAula(aula);
		oDocente.setDistrito(distrito);
		oDocente.setEdad(edad);
		oDocente.setFechaRegistro(fechaRegistro);
		oDocente.setNombres(nombres);
		oDocente.setTalla(talla);
		
		try {
			oDocenteDAOI.insertar(oDocente);
		} catch (Exception e) {
			System.out.println("NO SE PUDO INSERTAR");
			System.out.println(e.getMessage());
		}
	}
	
	public void editar() throws Exception{
		Docente oDocente = new Docente();
		oDocente.setIdDocente(getIdDocente());
		oDocente.setAula(aula);
		oDocente.setDistrito(distrito);
		oDocente.setEdad(edad);
		oDocente.setFechaRegistro(fechaRegistro);
		oDocente.setNombres(nombres);
		oDocente.setTalla(talla);
		
		try {
			oDocenteDAOI.editar(oDocente);
		} catch (Exception e) {
			System.out.println("NO SE PUDO EDITAR");
			System.out.println(e.getMessage());
		}
	}

	public void seleccionar_fila(ActionEvent e)
	{
		String fila=e.getComponent().getAttributes().get("attr_idDocente").toString();
		System.out.println("Id seleccionado:"+ fila);
		filaSeleccionada=Integer.parseInt(fila);
	}
	
	public void eliminar() throws Exception{
		Docente oDocente=new Docente();
		oDocente.setIdDocente(getFilaSeleccionada());
		oDocenteDAOI.eliminar(oDocente);
		
		buscar();
	}
	
	
	
	public int getIdDocente() {
		return idDocente;
	}

	public void setIdDocente(int idDocente) {
		this.idDocente = idDocente;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public double getTalla() {
		return talla;
	}

	public void setTalla(double talla) {
		this.talla = talla;
	}

	public Aula getAula() {
		return aula;
	}

	public void setAula(Aula aula) {
		this.aula = aula;
	}

	public Distrito getDistrito() {
		return distrito;
	}

	public void setDistrito(Distrito distrito) {
		this.distrito = distrito;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public DocenteDAOI getoDocenteDAOI() {
		return oDocenteDAOI;
	}

	public void setoDocenteDAOI(DocenteDAOI oDocenteDAOI) {
		this.oDocenteDAOI = oDocenteDAOI;
	}

	public List<Docente> getListaDocente() {
		return listaDocente;
	}

	public void setListaDocente(List<Docente> listaDocente) {
		this.listaDocente = listaDocente;
	}

	public List<Aula> getListaAula() {
		return listaAula;
	}

	public void setListaAula(List<Aula> listaAula) {
		this.listaAula = listaAula;
	}

	public List<Distrito> getListaDistrito() {
		return listaDistrito;
	}

	public void setListaDistrito(List<Distrito> listaDistrito) {
		this.listaDistrito = listaDistrito;
	}

	public int getListaDocente_size() {
		return listaDocente_size;
	}

	public void setListaDocente_size(int listaDocente_size) {
		this.listaDocente_size = listaDocente_size;
	}

	public int getFilaSeleccionada() {
		return filaSeleccionada;
	}

	public void setFilaSeleccionada(int filaSeleccionada) {
		this.filaSeleccionada = filaSeleccionada;
	}

	public String getOpcionSeleccionada() {
		return opcionSeleccionada;
	}

	public void setOpcionSeleccionada(String opcionSeleccionada) {
		this.opcionSeleccionada = opcionSeleccionada;
	}

	public boolean isSw_edit() {
		return sw_edit;
	}

	public void setSw_edit(boolean sw_edit) {
		this.sw_edit = sw_edit;
	}
	
	
	@PostConstruct
	public void init(){
		aula = new Aula();
		distrito= new Distrito();
	}
	
}
