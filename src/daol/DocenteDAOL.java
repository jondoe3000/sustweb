package daol;

import java.util.List;

import model.Aula;
import model.Distrito;
import model.Docente;

public interface DocenteDAOL {
	
	public abstract void insertar(Docente obj) throws Exception;
	public abstract void editar(Docente obj) throws Exception;
	public abstract void eliminar(Docente obj) throws Exception;
	
	public abstract List<Docente> buscarPorId(Docente obj) throws Exception;
	public abstract List<Docente> buscarPorAula(Docente obj) throws Exception;
	public abstract List<Docente> buscarPorDistrito(Docente obj) throws Exception;

	
	public abstract List<Distrito> listarDistritos() throws Exception;
	public abstract List<Aula> listarAulas() throws Exception;
}
